let express = require('express');
  
let app = express();

const port = process.env.PORT || 3000;

const bodyParser = require("body-parser");

const cookieParser = require('cookie-parser');

app.use(cookieParser());
app.use(bodyParser());
app.use(bodyParser.json());
    
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname + '/public/dist'));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/public/dist/index.html')
})
app.get('/test', function(req, res) {
  res.send("hello")
});

app.get('/service-worker.js', function(req, res) {
    res.setHeader('max-age','1000000')
    res.sendFile(__dirname + '/public/dist/service-worker.js')
})

app.listen(port);